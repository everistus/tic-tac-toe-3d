﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMovement : MonoBehaviour
{
    public bool isEnabled = true;
    public Vector2 Xpos;
    public Vector2 Ypos;
    public float Zpos = 1;
    // Use this for initialization
    void Start()
    {
        Cursor.visible = false;
    }

    Ray ray;
    RaycastHit hit;

    // Update is called once per frame
    void Update()
    {
        if (isEnabled == true)
        {
            Cursor.visible = false;
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = Zpos;
            //mousePos.x = Mathf.Clamp(mousePos.x, Xpos.x, Xpos.y);
            //mousePos.y = Mathf.Clamp(mousePos.y, Ypos.x, Ypos.y);
            transform.localPosition = Camera.main.ScreenToWorldPoint(mousePos);

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "point")
                {
                    hit.collider.GetComponent<PointSelector>().OnMouseEnter();
                }
            }
        }
        else Cursor.visible = true;
    }

}
