﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{


    public AudioMixer masterMixer;
    public AudioSource dropAS;

    public static AudioSource statDropAS;
    private void Start()
    {
        statDropAS = dropAS;
    }


    public void SetMasterSound(float soundLevel)
    {

        masterMixer.SetFloat("bgVol", Mathf.Log10(soundLevel) * 20);
    }

    public void SetAmbienceSound(float soundLevel)
    {

        masterMixer.SetFloat("ambVol", Mathf.Log10(soundLevel) * 20);
    }

    public void SetDrumsSound(float soundLevel)
    {

        masterMixer.SetFloat("drumVol", Mathf.Log10(soundLevel) * 20);
    }

    bool isToggle = true;
    public void ToggleSfx()
    {
        isToggle = !isToggle;
        if (isToggle == true)
        {
            masterMixer.SetFloat("sfxVol", 0);
        }
        else masterMixer.SetFloat("sfxVol", -80);

    }


    public static void playDrop()
    {
        statDropAS.Play();
    }
}
