﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Leaderboard : IComparable<Leaderboard>
{
    public int rank = 0;
    public string playerName = "";
    public int wins = 0;
    public int loss = 0;
    public int draw = 0;
    public int sprite = 0;
    public int sessionID = 0;
    public decimal playerScore = 0;



    public Leaderboard(int _rank, string _playerName, int _sprite, int _wins, int _loss, int _draw, int _sessionID, decimal _playerScore)
    {
        rank = _rank;
        playerName = _playerName;
        wins = _wins;
        loss = _loss;
        draw = _draw;
        sprite = _sprite;
        playerScore = _playerScore;
        sessionID = _sessionID;
    }
    public int CompareTo(Leaderboard other)
    {
        if (other == null)
            return 1;

        else
            return this.playerScore.CompareTo(other.playerScore);
    }


}
