﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{

    public Transform Player1pieceSpawn;
    public Transform Player2pieceSpawn;
    public GameObject p1Grip;
    public GameObject p2Grip;

    private CameraAnimation camAnim;
    private MoveTile moveTile;

    // Use this for initialization
    void Start()
    {
        camAnim = GetComponent<CameraAnimation>();
        moveTile = GetComponent<MoveTile>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Called when game starts or is reset
    /// </summary>
    /// <param name="piece"></param>
    public void InitialState(string piece)
    {
        if (piece == "X")
        {
            Debug.Log("Player starts with X");
            GameManager.Player1Piece = 'X';
            {
                Player1pieceSpawn.GetChild(0).GetChild(0).gameObject.SetActive(false);
                Player1pieceSpawn.GetChild(0).GetChild(1).gameObject.SetActive(true);
                p1Grip.transform.GetChild(0).gameObject.SetActive(false);
                p1Grip.transform.GetChild(1).gameObject.SetActive(true);
                //-----
                Player2pieceSpawn.GetChild(0).GetChild(0).gameObject.SetActive(true);
                Player2pieceSpawn.GetChild(0).GetChild(1).gameObject.SetActive(false);
                p2Grip.transform.GetChild(0).gameObject.SetActive(true);
                p2Grip.transform.GetChild(1).gameObject.SetActive(false);
            }
        }
        else if (piece == "O")
        {
            Debug.Log("Player starts with O");
            GameManager.Player1Piece = 'O';
            {
                Player1pieceSpawn.GetChild(0).GetChild(0).gameObject.SetActive(true);
                Player1pieceSpawn.GetChild(0).GetChild(1).gameObject.SetActive(false);
                p1Grip.transform.GetChild(0).gameObject.SetActive(true);
                p1Grip.transform.GetChild(1).gameObject.SetActive(false);
                //------
                Player2pieceSpawn.GetChild(0).GetChild(0).gameObject.SetActive(false);
                Player2pieceSpawn.GetChild(0).GetChild(1).gameObject.SetActive(true);
                p2Grip.transform.GetChild(0).gameObject.SetActive(false);
                p2Grip.transform.GetChild(1).gameObject.SetActive(true);
            }
        }

        GameManager.GameState = true;
        GameManager.InitialGameManagerSettings();
        GameManager.turn = GameManager.Turn.Player1Turn; //use settings
                                                         //  moveTile.Player2MovetoPoint("CPU");
        StartCoroutine(Delay());
        camAnim.GameStartAnim();
    }

    //Slight delay helps the code properly destroy previous GO's before adding new ones to the array
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.01f);
        moveTile.InitialMoveTileSettings();

    }
    public void ResetGame()
    {
        GameManager.GameState = true;
        GameManager.InitialGameManagerSettings();
    }
}
