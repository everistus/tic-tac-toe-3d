﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OverheadHover : MonoBehaviour
{
    public GameObject overHead;
    public GameObject overHeadPos;
    public GameObject overHeadAnchor;
    public TextMeshProUGUI lockIcon;
    private bool isLocked = false;
    //Extra bools for compensating for couroutine delay
    private static bool isDone = false;
    private bool hover = false;
    public void MouseHover()
    {
        if (isDone == false && isLocked == false && hover == false)
        {

            isDone = true;

            StartCoroutine(TweenHelper.LinearLerp(overHead, overHeadPos, Done));
            hover = true;
        }

    }


    public void MouseOut()
    {
        if (isDone == false && isLocked == false && hover == true)
        {
            isDone = true;

            StartCoroutine(TweenHelper.LinearLerp(overHead, overHeadAnchor, Done));
            hover = false;
        }

    }

    public static void Done()
    {
        isDone = false;
    }

    bool toggle = false;
    public void LockOverhead()
    {

        toggle = !toggle;
        isLocked = toggle;
        lockIcon.text = "\U0000f023"; //lock unicode

        if (isLocked == false)
        {
            lockIcon.text = "\U0000f09c"; //unlock unicode
            StartCoroutine(TweenHelper.LinearLerp(overHead, overHeadAnchor, Done));
        }
    }

}
