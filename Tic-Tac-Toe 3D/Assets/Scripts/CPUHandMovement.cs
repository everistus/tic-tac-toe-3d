﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPUHandMovement : MonoBehaviour
{

    public float handSpeed = 1f;
    static float t = 0;
    static int j = 0;
    Vector3 tempStart;
    private void Start()
    {
        tempStart = gameObject.transform.position;
    }
    /// <summary>
    /// Animates CPU hands for movement
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="obj2"></param>
    /// <param name="action"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    public IEnumerator Lerp(GameObject obj = null, GameObject obj2 = null, Action<int> action = null, int index = 0)
    {
        obj = gameObject;
        Vector3 destination = new Vector3(obj2.transform.position.x, obj.transform.position.y, obj2.transform.position.z);
        Vector3 startPos = gameObject.transform.position;
        while (true)
        {
            // animate the position of the game object...
            obj.transform.position = Vector3.Lerp(startPos, destination, t);

            // .. and increase the t interpolater
            t += handSpeed * Time.deltaTime;

            // in the opposite direction (ping-pong)
            if (t > handSpeed)
            {
                if (action != null)
                {
                    action(index);
                }
                action = null; // null action so it doesnt run it the 2nd time
                Vector3 temp = destination;
                destination = startPos;
                startPos = temp;
                t = 0;
                j++;

            }

            if (j == 2)
            {
                j = 0;
                obj.transform.position = tempStart; // resets to original position
                //switch turns
                GameManager.turn = GameManager.Turn.Player1Turn;
                break;
            }
            yield return null;

        }
    }
}
