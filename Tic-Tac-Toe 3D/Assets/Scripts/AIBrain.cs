﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIDifficulty { Novice, Intermediate, Expert }

public struct AIMove
{
    public int score;
    public int point;
    public AIMove(int _score, int _point)
    {
        score = _score;
        point = _point;
    }
}

public class AIBrain : MonoBehaviour
{

    private int[,] winCombos;
    private List<AIMove> aiMoves = new List<AIMove>();
    public AIDifficulty aiDifficulty = AIDifficulty.Novice;
    private Umpire umpire;
    private MoveTile moveTile;
    private List<GameObject> emptyPoints;
    [HideInInspector]
    public char[] testboard;
    private int count;

    private void Start()
    {
        umpire = GetComponent<Umpire>();
        moveTile = GetComponent<MoveTile>();
        winCombos = umpire.winCombos;

    }

    /// <summary>
    /// Calls the minimax function
    /// </summary>
    /// <returns></returns>
    public AIMove Minimax()
    {
        emptyPoints = new List<GameObject>(moveTile.emptyPoints);
        char[] board = new char[9]; System.Array.Copy(umpire.board, board, 9);
        aiMoves = new List<AIMove>();
        return GetBestMove(board, GameManager.Player2Piece, 0);
    }

    /// <summary>
    /// Returns the best move to minimax
    /// </summary>
    /// <param name="board"></param>
    /// <param name="player"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    private AIMove GetBestMove(char[] board, char player, int index)
    {
        // Novice uses a random spot
        AIMove randMove = new AIMove();
        randMove.score = 10;
        int val = Random.Range(0, emptyPoints.Count);
        randMove.point = System.Convert.ToInt32(emptyPoints[val].name);

        if (aiDifficulty == AIDifficulty.Intermediate)
        {
            //Terminal States

            AIMove terminalMove = new AIMove();
            if (CheckVictorySim(board, GameManager.Player1Piece) == true) // Human Player wins
            {
                terminalMove.score = -10;
                terminalMove.point = index;
                return terminalMove;
            }
            else if (CheckVictorySim(board, GameManager.Player2Piece) == true) // else if return victory for AI Player
            {
                terminalMove.score = 10;
                terminalMove.point = index;
                return terminalMove;
            }
            else if (emptyPoints.Count <= 0) // draw
            {
                terminalMove.score = 0;
                terminalMove.point = index;
                return terminalMove;
            }
            else // NO WLD
            {
                if (count >= 1)
                {
                    count++;
                    terminalMove.score = -50;
                    terminalMove.point = index;
                    return terminalMove;

                }
                else
                {
                    count++;
                }


            }

            //Recursive function is called at each iteration
            for (int i = 0; i < emptyPoints.Count; i++)
            {
                AIMove move = new AIMove();

                if (player == GameManager.Player2Piece)
                {
                    board[System.Convert.ToInt32(emptyPoints[i].name)] = player; // AI - adds a simulated piece on the test board to check for a Win Loss or Draw
                    player = GameManager.Player1Piece;
                    index = System.Convert.ToInt32(emptyPoints[i].name);
                    var result = GetBestMove(board, GameManager.Player2Piece, index);
                    move.score = result.score;
                    move.point = result.point;
                    this.aiMoves.Add(move);
                }
                if (player == GameManager.Player1Piece)
                {
                    board[System.Convert.ToInt32(emptyPoints[i].name)] = player; // Human - adds a simulated piece on the test board to check for a Win Loss or Draw
                    player = GameManager.Player2Piece;
                    index = System.Convert.ToInt32(emptyPoints[i].name);
                    var result = GetBestMove(board, GameManager.Player1Piece, index);
                    move.score = result.score;
                    move.point = result.point;
                    this.aiMoves.Add(move);
                }
                board[System.Convert.ToInt32(emptyPoints[i].name)] = 'Z';
            }


            //Logic for picking the best move for current player

            var bestMove = 0;

            if (player == GameManager.Player2Piece) // AI player
            {
                int bestScore = -10000;
                for (int i = 0; i < aiMoves.Count; i++)
                {
                    if (aiMoves[i].score > bestScore)
                    {
                        bestMove = i;
                        bestScore = aiMoves[i].score;

                    }
                }
            }
            else if (player == GameManager.Player1Piece) // Human Player
            {
                int bestScore = 10000;
                for (int i = 0; i < aiMoves.Count; i++)
                {
                    if (aiMoves[i].score < bestScore)
                    {
                        bestMove = i;
                        bestScore = aiMoves[i].score;
                    }
                }
            }

            // Zero the count value
            count = 0;
            //return the best move
            return aiMoves[bestMove];
        }



        return randMove;

    }

    /// <summary>
    /// Checks the simulated board for a win and returns the bool
    /// </summary>
    /// <param name="board"></param>
    /// <param name="playerPiece"></param>
    /// <returns></returns>
    private bool CheckVictorySim(char[] board, char playerPiece)
    {
        testboard = board;
        char piece = playerPiece;
        for (int i = 0; i < 8; i++)
        {
            if (board[winCombos[i, 0]] == piece && board[winCombos[i, 1]] == piece && board[winCombos[i, 2]] == piece)
            {
                Debug.Log("<color=green> Player wins</color> at point ");
                return true;
            }

        }

        return false;

    }


}
