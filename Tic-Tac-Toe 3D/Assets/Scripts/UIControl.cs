﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIControl : MonoBehaviour
{
    [SerializeField]
    public GameObject[] panels;
    public GameObject _GameManager;
    public GameObject settingsPanel;
    public GameObject instructionsPanel;
    [Space]
    public Image p1Avatar;
    public Image p2Avatar;
    [Space]
    public TextMeshProUGUI P1Name;
    public TextMeshProUGUI P2Name;
    [Space]
    public TextMeshProUGUI P1Score;
    public TextMeshProUGUI P2Score;
    [Space]
    public TextMeshProUGUI AILevel;
    public GameObject umpireCard;
    public TextMeshProUGUI winText;
    public GameObject newScoreCard;
    public GameObject vfxBlock;

    private GameOptions gameOptions;

    private void Start()
    {
        gameOptions = GetComponent<GameOptions>();
    }

    #region // Navigations Panel Controls
    private int step = 0;
    /// <summary>
    ///  Move forward in panel array
    /// </summary>

    public void FwdBtn()  // Called by Next Button
    {

        if (step < panels.Length - 1)
        {
            step++;
            panels[step].SetActive(true);
            panels[step - 1].SetActive(false);


        }
        else
        {
            panels[step].SetActive(false);
            step = 0;
            panels[step].SetActive(true);

        }
    }
    /// <summary>
    /// Move back in panels array
    /// </summary>
    public void Backbtn() // Called by Previous Button
    {
        if (step > 0)
        {
            step--;
            panels[step].SetActive(true);
            panels[step + 1].SetActive(false);


        }
        else
        {
            panels[step].SetActive(false);
            step = panels.Length - 1;
            panels[step].SetActive(true);

        }
    }
    /// <summary>
    /// Toggles On or Off Current Panel
    /// </summary>
    public void CurrentPanelToggle(bool isToggle1)
    {
        panels[step].SetActive(isToggle1);
    }
    /// <summary>
    /// Toggles On or Off Settings Panel
    /// </summary>
    /// <param name="toggle2"></param>
    public void SettingsToggle(bool toggle2)
    {
        if (toggle2 == true) settingsPanel.SetActive(toggle2);
        else if (toggle2 == false) settingsPanel.SetActive(toggle2);
    }




    #endregion

    public void SetOverHead(string player)
    {
        p1Avatar.sprite = gameOptions.player1Image.sprite;
        p2Avatar.sprite = gameOptions.aiImage.sprite;

        P1Name.text = gameOptions.player1Input.text;
        if (player == "CPU") P2Name.text = gameOptions.aiInput.text;

        P1Score.text = 0.ToString();
        P2Score.text = 0.ToString();

        AILevel.text = gameOptions.levels[gameOptions.levelCounter] + " AI";
    }

    public void UpdateTally(int p1score, int p2score, string _winText)
    {
        P1Score.text = p1score.ToString();
        P2Score.text = p2score.ToString();
        winText.text = _winText;
    }

    public void ShowWinCard(bool isVisible)
    {
        umpireCard.SetActive(isVisible);
    }

    public void ShowHSCard()
    {
        StartCoroutine(ShowCard());
    }
    IEnumerator ShowCard()
    {
        newScoreCard.SetActive(true);
        yield return new WaitForSeconds(2f);
        newScoreCard.SetActive(false);
    }
    public void UIControlReset()
    {
        ShowWinCard(false);
    }

    //Toggle  instructions panel
    bool isActive;
    public void ShowInstructions()
    {

        isActive = !isActive;
        instructionsPanel.SetActive(isActive);

    }

    //Toggle vfx
    bool isActive2 = true;
    public void ToggleVfx()
    {

        isActive2 = !isActive2;
        vfxBlock.SetActive(isActive2);

    }

}
