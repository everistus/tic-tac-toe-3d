﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


/// <summary>
/// Options are set before the games start and cannot be changed until player goes back to main menu 
/// </summary>
public class GameOptions : MonoBehaviour
{

    public GameObject gameManager;


    [Header("Player1 Options")]
    public TextMeshProUGUI player1Input;
    public Image player1Image;
    public Image player1Avatar;

    [Header("AI Options")]
    public TMP_InputField aiInput;
    public Image aiImage;
    public TextMeshProUGUI levelText;

    public Sprite[] femaleChar;
    public Sprite[] maleChar;

    [Header("Piece Selection")]
    public GameObject[] piece;



    string[] femaleNames = { "Emily", "Hannah", "Ashley", "Sarah", "Emma" };
    string[] maleNames = { "Jacob", "Michael", "Matthew", "Joshua", "Chris" };
    [HideInInspector]
    public string[] levels = { "NOVICE", "INTERMEDIATE" };

    int counter;
    int P1Counter;
    [HideInInspector]
    public int levelCounter;

    private GameEvents gameEvents;
    private AIBrain aIBrain;

    private UIControl uIControl;
    private StatsManager statsManager;


    private void Start()
    {
        gameEvents = gameManager.GetComponent<GameEvents>();
        aIBrain = gameManager.GetComponent<AIBrain>();
        uIControl = GetComponent<UIControl>();
        statsManager = gameManager.GetComponent<StatsManager>();

        DefaultOptions();
    }

    #region // Piece Controls
    /// <summary>
    /// Next Piece
    /// </summary>
    public void PieceNext()
    {
        if (counter < piece.Length - 1)
        {
            counter++;
            piece[counter].SetActive(true);
            piece[counter - 1].SetActive(false);


        }
        else
        {
            piece[counter].SetActive(false);
            counter = 0;
            piece[counter].SetActive(true);

        }
    }

    /// <summary>
    /// Previous piece
    /// </summary>
    public void PiecePrev()
    {
        if (counter > 0)
        {
            counter--;
            piece[counter].SetActive(true);
            piece[counter + 1].SetActive(false);


        }
        else
        {
            piece[counter].SetActive(false);
            counter = piece.Length - 1;
            piece[counter].SetActive(true);

        }
    }

    #endregion


    #region // Avatar Controls
    public void AvatarNext(bool isPlayer1 = true)  // Called by Next Button
    {
        if (isPlayer1 == true)
        {
            if (P1Counter < femaleChar.Length - 1 && P1Counter >= 0)
            {
                P1Counter++;
                player1Avatar.sprite = femaleChar[P1Counter];
                player1Image.sprite = player1Avatar.sprite;
            }
            else if (P1Counter < -1)
            {
                P1Counter++;
                player1Avatar.sprite = maleChar[Mathf.Abs(P1Counter + maleChar.Length)];
                player1Image.sprite = player1Avatar.sprite;
            }
            else if (P1Counter < 0)
            {
                P1Counter++;
                player1Avatar.sprite = femaleChar[P1Counter];
                player1Image.sprite = player1Avatar.sprite;
            }
            else
            {

                if (P1Counter > (femaleChar.Length - 1 + maleChar.Length - 1))
                {
                    P1Counter = 0;
                    player1Avatar.sprite = femaleChar[P1Counter];
                    player1Image.sprite = player1Avatar.sprite;
                }
                else
                {
                    P1Counter++;
                    player1Avatar.sprite = maleChar[P1Counter - femaleChar.Length];
                    player1Image.sprite = player1Avatar.sprite;
                }



            }

        }

    }

    public void AvatarPrev(bool isPlayer1 = true) // Called by Previous Button
    {
        if (isPlayer1 == true)
        {
            P1Counter--;
            if (P1Counter >= 0 && P1Counter <= (femaleChar.Length - 1))
            {
                player1Avatar.sprite = femaleChar[P1Counter];
                player1Image.sprite = player1Avatar.sprite;


            }
            else if (P1Counter > femaleChar.Length - 1 && P1Counter <= (femaleChar.Length - 1 + maleChar.Length - 1))
            {
                player1Avatar.sprite = maleChar[P1Counter - femaleChar.Length];
                player1Image.sprite = player1Avatar.sprite;


            }
            else if (P1Counter < 0)
            {
                player1Avatar.sprite = maleChar[Mathf.Abs(P1Counter + maleChar.Length)];
                player1Image.sprite = player1Avatar.sprite;
                if (P1Counter == -(maleChar.Length + 1))
                {
                    P1Counter = femaleChar.Length - 1;
                    player1Avatar.sprite = femaleChar[P1Counter];
                    player1Image.sprite = player1Avatar.sprite;
                }
            }
        }
    }

    #endregion

    #region // AI Level Controls

    /// <summary>
    /// Next level
    /// </summary>
    public void LevelNext()
    {
        if (levelCounter < piece.Length - 1)
        {
            levelCounter++;
            levelText.text = levels[levelCounter];
            aIBrain.aiDifficulty = (AIDifficulty)levelCounter;



        }
        else
        {
            levelCounter = 0;
            levelText.text = levels[levelCounter];
            aIBrain.aiDifficulty = (AIDifficulty)levelCounter;

        }
    }

    /// <summary>
    /// Previous level
    /// </summary>
    public void LevelPrev()
    {
        if (levelCounter > 0)
        {
            levelCounter--;
            levelText.text = levels[levelCounter];
            aIBrain.aiDifficulty = (AIDifficulty)levelCounter;


        }
        else
        {
            levelCounter = piece.Length - 1;
            levelText.text = levels[levelCounter];
            aIBrain.aiDifficulty = (AIDifficulty)levelCounter;

        }
    }
    #endregion

    /// <summary>
    /// Game default options
    /// </summary>
    public void DefaultOptions()
    {
        counter = 0;
        P1Counter = 0;

        piece[counter].SetActive(true);
        piece[counter + 1].SetActive(false);


        //AI
        int AIgender = Random.Range(0, 2);

        if (AIgender == 0)
        {
            int id = Random.Range(0, 5);
            aiImage.sprite = femaleChar[id];
            aiInput.text = femaleNames[id];
        }
        else
        {
            int id = Random.Range(0, 5);
            aiImage.sprite = maleChar[id];
            aiInput.text = maleNames[id];
        }

        //Player1

        int P1gender = Random.Range(0, 2);
        if (P1gender == 0)
        {
            int id = Random.Range(0, 5);
            P1Counter = id;
            player1Avatar.sprite = femaleChar[id];
            player1Image.sprite = player1Avatar.sprite;
        }
        else
        {
            int id = Random.Range(0, 5);
            P1Counter = id + maleChar.Length;
            player1Avatar.sprite = maleChar[id];
            player1Image.sprite = player1Avatar.sprite;
        }


    }

    /// <summary>
    /// New options on submit
    /// </summary>
    public void SubmitOptions()
    {
        if (counter == 0)
        {
            gameEvents.InitialState("X");

        }
        else if (counter == 1)
        {
            gameEvents.InitialState("O");
        }

        uIControl.SetOverHead("CPU");

    }

    /// <summary>
    /// Sends game options such as player name and image to stats script
    /// </summary>
    public void UpdateStat()
    {
        int spriteIndex = 0;
        if (P1Counter < 0)
        {
            spriteIndex = P1Counter + (femaleChar.Length + maleChar.Length);
        }
        else spriteIndex = P1Counter;

        statsManager.SetPlayerStat(player1Input.text, spriteIndex);
    }





    /*
     * ai level
     * timer
     * 
     * */
}
