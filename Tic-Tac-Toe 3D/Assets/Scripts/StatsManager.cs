﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{


    private int player1Score = 0;
    private int player2Score = 0;
    private int playerDraw = 0;
    internal string playerName = "";
    internal int playerAvatar;

    int wins;
    int loss;
    int draw;
    public decimal ratio;

    public GameObject UIManager;
    LeaderboardManager lbManager;
    UIControl uIControl;


    private void Start()
    {
        uIControl = UIManager.GetComponent<UIControl>();
        lbManager = uIControl.GetComponent<LeaderboardManager>();
    }

    /// <summary>
    /// Sets the player tally for player 1 and 2
    /// </summary>
    /// <param name="player1"></param>
    /// <param name="player2"></param>
    public void SetScore(int player1, int player2, int _playerDraw, string winText)
    {
        player1Score += player1;
        player2Score += player2;
        playerDraw += _playerDraw;

        uIControl.UpdateTally(player1Score, player2Score, winText);
        uIControl.ShowWinCard(true);
        if (player1 == 1)
        {
            UpdateLeaderboard();
        }

    }

    /// <summary>
    /// Updates playerName and Sprite
    /// </summary>
    /// <param name="name"></param>
    /// <param name="spriteIndex"></param>
    public void SetPlayerStat(string name, int spriteIndex)
    {
        playerName = name;
        playerAvatar = spriteIndex;

    }


    /// <summary>
    /// Calculates WLD and ratio
    /// </summary>
    private void CalculateStats()
    {
        wins = player1Score;
        loss = player2Score;
        draw = playerDraw;

        if (loss == 0)
        {

            ratio = (wins / 1.00m);
            ratio = (decimal)Math.Round(ratio, 2);
        }
        else
        {
            ratio = wins * 1.00m / loss;
            ratio = (decimal)Math.Round(ratio, 2);
        }
    }

    public void UpdateLeaderboard()
    {
        CalculateStats();
        lbManager.SortLeaderBoard(1, playerName, playerAvatar, wins, loss, draw, 0, ratio);
    }



}
