﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static bool gameState = false;
    public static bool gameWon = false;
    /// <summary>
    /// Leaderboard data received from server
    /// </summary>
    public static bool leaderBoardReceived = false;
    /// <summary>
    /// Used to keep track of current gameplay session and update just a single spot for a player instead of repeating
    /// </summary>
    public static bool leaderboardSession = false;

    public enum Turn { Player1Turn, Player2Turn }

    public static char Player1Piece = 'X';
    public static char Player2Piece = 'O';

    public static Turn turn = Turn.Player1Turn;

    private void Start()
    {
        //Remove 
    }

    public static bool GameState
    {
        get
        {
            return gameState;
        }

        set
        {
            gameState = value;
        }
    }
    /// <summary>
    /// Called when game starts or is reset
    /// </summary>
    public static void InitialGameManagerSettings()
    {
        if (Player1Piece == 'X')
        {
            Player2Piece = 'O';
        }
        else Player2Piece = 'X';
    }
}
