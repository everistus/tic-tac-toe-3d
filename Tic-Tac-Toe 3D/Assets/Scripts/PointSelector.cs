﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSelector : MonoBehaviour
{

    public static int pointCount { get; set; }
    // private static MoveTile moveTile;


    private void Awake()
    {
        //   moveTile = MoveTile._instance;
    }
    private void Start()
    {
        pointCount = 9;
    }

    private void OnMouseDown()
    {
        if (GameManager.GameState == true)
        {
            if (GameManager.turn == GameManager.Turn.Player1Turn)
            {
                if (pointCount > 0)
                {
                    SoundManager.playDrop();
                    MoveTile._instance.Player1MovetoPoint("p1", int.Parse(gameObject.name));
                    gameObject.GetComponent<BoxCollider>().enabled = false;
                    pointCount--;
                }
            }
        }


    }

    public void OnMouseEnter()
    {
        if (GameManager.GameState == true)
        {
            GetComponent<MeshRenderer>().enabled = true;
        }

    }

    public void OnMouseExit()
    {
        if (GameManager.GameState == true)
        {
            GetComponent<MeshRenderer>().enabled = false;
        }

    }

    public void OnAIMouseDown()
    {
        if (GameManager.GameState == true)
        {
            if (pointCount > 0)
            {
                gameObject.GetComponent<BoxCollider>().enabled = false;
                pointCount--;
            }

        }

    }

    public static void PointSelectorReset()
    {
        pointCount = 9;
        foreach (var item in MoveTile._instance.currentPoints)
        {
            item.GetComponent<BoxCollider>().enabled = true;
        }

    }
}
