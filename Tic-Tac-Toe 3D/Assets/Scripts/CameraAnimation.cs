﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour
{

    public GameObject GameCamera;
    public GameObject PanelsCamera;
    public GameObject[] playerHands;

    public Animator anim;

    private Vector3 savePos;
    // Use this for initialization
    void Start()
    {
        savePos = PanelsCamera.transform.position;
        StartCoroutine(AnimCam());
    }

    // Update is called once per frame

    void Update()
    {


    }

    public void GameStartAnim()
    {
        anim.Play("coverOpen");
        StopAllCoroutines();
        PanelsCamera.transform.position = savePos;
        StartCoroutine(StartGameCam());
    }

    float step2 = 0;
    /// <summary>
    /// Animated from the panels camera to the main gameplay camera
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartGameCam()
    {

        do
        {
            PanelsCamera.transform.position = Vector3.Lerp(PanelsCamera.transform.position, GameCamera.transform.position, step2);
            PanelsCamera.transform.rotation = Quaternion.Lerp(PanelsCamera.transform.rotation, GameCamera.transform.rotation, step2);
            step2 += Time.deltaTime / 8;
            if (step2 > .2f)
            {
                PanelsCamera.SetActive(false);
                GameCamera.SetActive(true);
                foreach (var item in playerHands)
                {
                    item.GetComponent<HandMovement>().enabled = true;
                }
            }
            yield return null;
        } while (true);



    }

    float step1 = 0;
    int j = 0;
    /// <summary>
    /// Animate camera on start screen
    /// </summary>
    /// <returns></returns>
    private IEnumerator AnimCam()
    {
        Vector3 destination = GetRandomVector(savePos);
        Vector3 startPos = savePos;
        do
        {
            if (j < 8)
            {
                PanelsCamera.transform.position = Vector3.Lerp(startPos, destination, step1);
                step1 += Time.deltaTime / 10;
                if (step1 > 2f)
                {
                    j++;
                    Vector3 temp = destination;
                    destination = GetRandomVector(startPos);
                    startPos = temp;
                    step1 = 0;
                }
            }
            else // just to make sure the pos changes doesnt get too crazy reset to original
            {
                PanelsCamera.transform.position = Vector3.Lerp(PanelsCamera.transform.position, savePos, step1);
                destination = GetRandomVector(savePos);
                startPos = savePos;
                step1 += Time.deltaTime / 10;
                if (step1 > 2f) { j = 0; step1 = 0; }
            }

            yield return null;
        } while (true);



        //  float step = 1 * Time.deltaTime;
        //  PanelsCamera.transform.position = Vector3.MoveTowards(PanelsCamera.transform.position, GameCamera.transform.position, step);

    }

    private Vector3 GetRandomVector(Vector3 modVector)
    {
        float xfloat = Random.Range(-.3f, .3f); float yfloat = Random.Range(-.3f, .3f); float zfloat = Random.Range(-.3f, .3f);
        Vector3 mv = new Vector3(modVector.x + xfloat, modVector.y + yfloat, modVector.z + zfloat);

        return mv;
    }
}
