﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class WWWFormLeaderboard
{


    string leaderboard_url = "http://comphonia.000webhostapp.com/leaderboard.php";

    /// <summary>
    /// METHOD: saves leaderboard data to server
    /// </summary>
    /// <param name="leaderboardList"></param>
    /// <returns></returns>
    public IEnumerator SaveLeaderboard(Leaderboard[] leaderboardList)
    {
        WWWForm form = new WWWForm();

        // This block breaks up our array values and passes them to the server as a single string
        // you can use json or any other method in saving files to a server but unity form only accepts string so I had to be creative

        string leaderboard = "";
        for (int i = 0; i < leaderboardList.Length; i++)
        {

            if (leaderboardList[i].playerName != null && leaderboardList[i].playerName != "")
            {
                leaderboard += leaderboardList[i].rank + ",";
                leaderboard += leaderboardList[i].playerName.Trim() + ",";
                leaderboard += leaderboardList[i].sprite + ",";
                leaderboard += leaderboardList[i].wins + ",";
                leaderboard += leaderboardList[i].loss + ",";
                leaderboard += leaderboardList[i].draw + ",";
                leaderboard += leaderboardList[i].sessionID + ",";

                if (i == leaderboardList.Length - 1)
                {
                    leaderboard += leaderboardList[i].playerScore.ToString();
                }
                else leaderboard += leaderboardList[i].playerScore.ToString() + ",";
            }
        }
        form.AddField("leaderboard", leaderboard);



        var download = UnityWebRequest.Post(leaderboard_url, form);
        yield return download.SendWebRequest();

        if (download.isNetworkError || download.isHttpError)
        {
            Debug.Log("Error downloading: " + download.error);
        }
        else
        {
            Debug.Log(download.downloadHandler.text);
        }
    }

    public IEnumerator LoadLeaderboard()
    {
        LeaderboardManager lbManager = LeaderboardManager._instance;
        WWWForm form = new WWWForm();
        form.AddField("loadleaderboard", "true");

        var download = UnityWebRequest.Post(leaderboard_url, form);
        yield return download.SendWebRequest();

        if (download.isNetworkError || download.isHttpError)
        {
            Debug.Log("Error downloading: " + download.error);
            GameManager.leaderBoardReceived = false;
        }
        else
        {
            // Return received leaderboard
            GameManager.leaderBoardReceived = true;
            lbManager.SetLeaderboard(BuildLeaderboard(download.downloadHandler.text));
        }
    }

    /// <summary>
    /// Constructs a leaderboard array object from string received
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    private Leaderboard[] BuildLeaderboard(string text)
    {
        // Debug
        string[] a = text.Trim().Split(',');
        Leaderboard[] leaderboard = new Leaderboard[8];
        int j = 0;

        for (int i = 0; i < leaderboard.Length; i++)
        {
            try
            {
                leaderboard[i] = new Leaderboard(Convert.ToInt32(a[j]), a[j + 1], Convert.ToInt32(a[j + 2]), Convert.ToInt32(a[j + 3]), Convert.ToInt32(a[j + 4]), Convert.ToInt32(a[j + 5]), Convert.ToInt32(a[j + 6]), Convert.ToDecimal(a[j + 7]));
            }
            catch (Exception)
            {
                leaderboard[i] = new Leaderboard(0, "", 0, 0, 0, 0, 0, 0);
            }

            j += 8;
        }

        return leaderboard;
    }
}

