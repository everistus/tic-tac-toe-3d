﻿using System;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum SaveDestination { Server, Local }
public class LeaderboardManager : MonoBehaviour
{
    public SaveDestination saveDestination;

    public GameObject leaderboardPanel;

    public GameObject[] leaderboardPositions;

    private Leaderboard[] leaderboardList = new Leaderboard[8];

    public static LeaderboardManager _instance = null;
    GameOptions gameOptions;
    UIControl uIControl;
    UIHover uIHover;
    WWWFormLeaderboard formLeaderboard = new WWWFormLeaderboard();

    private int sessionID;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance = this)
        {
            Destroy(gameObject.GetComponent<MoveTile>());
        }

        // DontDestroyOnLoad(gameObject);

    }

    private void Start()
    {
        gameOptions = GetComponent<GameOptions>();
        uIControl = GetComponent<UIControl>();
        uIHover = GetComponent<UIHover>();

        sessionID = GenerateSessionID();
        if (saveDestination == SaveDestination.Server)
        {
            StartCoroutine(formLeaderboard.LoadLeaderboard());
        }
        else
        {
            ReadFile();
        }

    }

    private int GenerateSessionID()
    {
        int ID = UnityEngine.Random.Range(9, 99999);
        return ID;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Tab"))
        {
            ShowLB();
        }
    }

    /// <summary>
    ///  Checks for the smallest score on the leaderboard, replaces it, sorts the array and the puts the values on the leaderboard
    /// </summary>
    public void SortLeaderBoard(int rank, string name, int sprite, int wins, int loss, int draw, int _sessionID, decimal currentScore)
    {

        if (GameManager.leaderboardSession == false)
        {
            //Replace smallest score with current score if current score is >
            decimal smallest = currentScore;
            int index = 0;
            for (int i = 0; i < leaderboardList.Length; i++)
            {
                if (smallest > leaderboardList[i].playerScore)
                {
                    smallest = leaderboardList[i].playerScore;
                    index = i;
                }
            }
            {
                if (currentScore > smallest)
                {
                    GameManager.leaderboardSession = true;
                    uIControl.ShowHSCard();
                    rank = index + 1;
                    leaderboardList[index] = new Leaderboard(rank, name, sprite, wins, loss, draw, this.sessionID, currentScore); //adds leaderboard item

                    //  leaderboardPositions[index].transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].rank.ToString();
                    leaderboardPositions[index].transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].playerName;
                    leaderboardPositions[index].transform.GetChild(1).GetChild(1).GetComponent<Image>().sprite = GetSprite(leaderboardList[index].sprite);
                    leaderboardPositions[index].transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].wins.ToString();
                    leaderboardPositions[index].transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].loss.ToString();
                    leaderboardPositions[index].transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].draw.ToString();
                    leaderboardPositions[index].transform.GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = String.Format("{0:0.00}", leaderboardList[index].playerScore);

                }

            }
            Array.Sort(leaderboardList);
            Array.Reverse(leaderboardList);


            if (saveDestination == SaveDestination.Server)
            {
                StartCoroutine(formLeaderboard.SaveLeaderboard(leaderboardList));
                StartCoroutine(formLeaderboard.LoadLeaderboard());
            }
            else
            {
                WriteFile();  //writes leaderboard item to text file
                              //  ReadFile();
            }


        }
        else // find object with session ID, update its values and sort the lb again to avoid duplicates in session
        {
            for (int index = 0; index < leaderboardList.Length; index++)
            {
                if (leaderboardList[index].sessionID == this.sessionID)
                {
                    leaderboardList[index] = new Leaderboard(rank, name, sprite, wins, loss, draw, this.sessionID, currentScore); //adds leaderboard item
                                                                                                                                  //  leaderboardPositions[index].transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].rank.ToString();
                    leaderboardPositions[index].transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].playerName;
                    leaderboardPositions[index].transform.GetChild(1).GetChild(1).GetComponent<Image>().sprite = GetSprite(leaderboardList[index].sprite);
                    leaderboardPositions[index].transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].wins.ToString();
                    leaderboardPositions[index].transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].loss.ToString();
                    leaderboardPositions[index].transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[index].draw.ToString();
                    leaderboardPositions[index].transform.GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = String.Format("{0:0.00}", leaderboardList[index].playerScore);
                }


            }

        }

        Array.Sort(leaderboardList);
        Array.Reverse(leaderboardList);
        if (saveDestination == SaveDestination.Server)
        {
            StartCoroutine(formLeaderboard.SaveLeaderboard(leaderboardList));
            StartCoroutine(formLeaderboard.LoadLeaderboard());
        }
        else
        {
            WriteFile();  //writes leaderboard item to text file
            ReadFile();
        }
    }

    /// <summary>
    /// Receives a leaderboard array from WWWFormLeaderboard().loadLeaderboard and assigns the values to the leaderboard positions
    /// </summary>
    /// <param name="leaderboard"></param>
    public void SetLeaderboard(Leaderboard[] leaderboard)
    {

        for (int i = 0; i < leaderboard.Length; i++)
        {
            //If error assign catch
            try
            {
                leaderboardList[i] = new Leaderboard(leaderboard[i].rank, leaderboard[i].playerName, leaderboard[i].sprite, leaderboard[i].wins, leaderboard[i].loss, leaderboard[i].draw, leaderboard[i].sessionID, leaderboard[i].playerScore);
            }
            catch (Exception)
            {

                leaderboardList[i] = new Leaderboard(0, "", 0, 0, 0, 0, 0, 0);
            }
            //   leaderboardPositions[i].transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].rank.ToString();
            leaderboardPositions[i].transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].playerName;
            leaderboardPositions[i].transform.GetChild(1).GetChild(1).GetComponent<Image>().sprite = GetSprite(leaderboardList[i].sprite);
            leaderboardPositions[i].transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].wins.ToString();
            leaderboardPositions[i].transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].loss.ToString();
            leaderboardPositions[i].transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].draw.ToString();
            leaderboardPositions[i].transform.GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = String.Format("{0:0.00}", leaderboardList[i].playerScore);
        }
    }


    private Sprite GetSprite(int spriteIndex)
    {
        if (spriteIndex > gameOptions.femaleChar.Length - 1)
        {
            spriteIndex = spriteIndex - gameOptions.maleChar.Length;
            return gameOptions.maleChar[spriteIndex];
        }
        return gameOptions.femaleChar[spriteIndex];
    }

    //Toggle UIButton for testing - display leaderboard
    bool isActive;
    public void ShowLB()
    {
        if (saveDestination == SaveDestination.Server)
        {
            StartCoroutine(formLeaderboard.LoadLeaderboard());
        }
        else ReadFile();

        isActive = !isActive;
        leaderboardPanel.SetActive(isActive);
        if (isActive == false) uIHover.MouseOut();

    }


    #region // Writing and reading data to text file in the Resources folder
    /// <summary>
    /// Saves leaderboard data in text file
    /// </summary>
    private void WriteFile()
    {
        string path = "Assets/Resources/data.txt";

        //Write some text to the data.txt file
        StreamWriter writer = new StreamWriter(path, false);

        for (int i = 0; i < leaderboardList.Length; i++)
        {
            if (leaderboardList[i].playerName != null && leaderboardList[i].playerName != "")
            {
                writer.WriteLine(leaderboardList[i].rank);
                writer.WriteLine(leaderboardList[i].playerName);
                writer.WriteLine(leaderboardList[i].sprite);
                writer.WriteLine(leaderboardList[i].wins);
                writer.WriteLine(leaderboardList[i].loss);
                writer.WriteLine(leaderboardList[i].draw);
                writer.WriteLine(leaderboardList[i].sessionID);
                writer.WriteLine(leaderboardList[i].playerScore);
            }
        }

        writer.Close();

        //Re-import the file to update the reference in the editor
        // AssetDatabase.ImportAsset(path);
        //  TextAsset asset = (TextAsset)Resources.Load("data");
    }

    /// <summary>
    /// Reads  leaderboard data from text file and add it to the leaderboard
    /// </summary>
    public void ReadFile()
    {
        string path = "Assets/Resources/data.txt";

        //Read the text from directly from the data.txt file
        StreamReader reader = new StreamReader(path);
        for (int i = 0; i < leaderboardList.Length; i++)
        {
            //If name or score is null, assign catch
            try
            {
                leaderboardList[i] = new Leaderboard(int.Parse(reader.ReadLine()), reader.ReadLine(), int.Parse(reader.ReadLine()), int.Parse(reader.ReadLine()), int.Parse(reader.ReadLine()), int.Parse(reader.ReadLine()), int.Parse(reader.ReadLine()), Convert.ToDecimal(reader.ReadLine()));
            }
            catch (Exception)
            {

                leaderboardList[i] = new Leaderboard(0, "", 0, 0, 0, 0, 0, 0);
            }

            // leaderboardPositions[i].transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].rank.ToString();
            leaderboardPositions[i].transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].playerName;
            leaderboardPositions[i].transform.GetChild(1).GetChild(1).GetComponent<Image>().sprite = GetSprite(leaderboardList[i].sprite);
            leaderboardPositions[i].transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].wins.ToString();
            leaderboardPositions[i].transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].loss.ToString();
            leaderboardPositions[i].transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].draw.ToString();
            // leaderboardPositions[i].transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>().text = leaderboardList[i].sessionID.ToString();
            leaderboardPositions[i].transform.GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = String.Format("{0:0.00}", leaderboardList[i].playerScore);
        }
        reader.Close();
    }

    #endregion
}