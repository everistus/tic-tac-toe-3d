﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CLASS: Sets the rules for determining player or win WIN/LOSE/DRAW
/// </summary>
public class Umpire : MonoBehaviour
{

    #region
    /*
     * Sample Gameplay
     * ===============
     * 
     * 0    0   0
     * 0    0   0      Empty board Spaces
     * 0    0   0
     * 
     * 0    0   1
     * 0    0   0       Player makes move at array rc[0,2]
     * 0    0   0
     * 
     * 0    0   1
     * 0    2   0       Ai makes move at array rc[1,1]
     * 0    0   0
     * 
     * 2    1   1
     * 0    2   1       Ai wins !! with winning points [0,0],[1,1],[2,2]
     * 0    1   2
     *
     */

    #endregion

    public Material highLightMaterialP1;
    public Material highLightMaterialP2;
    public Material highLightMaterialP3;
    [HideInInspector]
    public char[] board = new char[9];

    MoveTile moveTile;
    GameEvents gameEvents;
    StatsManager statsManager;
    UIControl uIControl;

    enum WLD { none, win, draw }
    WLD wld = WLD.none;

    //8Rows , 3 Columns [7,2]
    /// <summary>
    /// possible horizontal, vertical and diagonal combinations for wins
    /// </summary>
    public int[,] winCombos = new int[8, 3] {
        { 0, 1, 2 }, // winCombos[0,0] , [0,1], [0,2]
        { 3, 4, 5 },
        { 6, 7, 8 },
        { 0, 3, 6 },
        { 1, 4, 7 },
        { 2, 5, 8 },
        { 0, 4, 8 },
        { 6, 4, 2 } };   // [7,0] , [7,1] , [7,2]

    private void Start()
    {
        moveTile = GetComponent<MoveTile>();
        gameEvents = GetComponent<GameEvents>();
        statsManager = GetComponent<StatsManager>();
        uIControl = statsManager.UIManager.GetComponent<UIControl>();

    }
    /// <summary>
    /// Adds a played piece and point to the board
    /// </summary>
    /// <param name="piece"></param>
    /// <param name="point"></param>
    public void AddBoard(char piece, int point)
    {
        board[point] = piece;
        CheckWinner(piece);
    }
    /// <summary>
    /// iterates to 9 for sum of games played
    /// </summary>
    int totalTurns = 0;
    /// <summary>
    /// used to get winning points, passes it to MoveTile for highlighting the pieces
    /// </summary>
    int[] winPoints = new int[3];

    /// <summary>
    /// Checks pieces in board positions for winners
    /// </summary>
    /// <param name="piece"></param>
    private void CheckWinner(char piece)
    {

        for (int i = 0; i < 8; i++)
        {
            if (piece == GameManager.Player1Piece)
            {
                if (board[winCombos[i, 0]] == piece && board[winCombos[i, 1]] == piece && board[winCombos[i, 2]] == piece)
                {
                    Debug.Log("<color=green> Player1 wins</color>");
                    //stores wiining points to be higlighted in an array
                    winPoints[0] = winCombos[i, 0];
                    winPoints[1] = winCombos[i, 1];
                    winPoints[2] = winCombos[i, 2];
                    print("WIN POINTS: " + winPoints[0] + "," + winPoints[1] + "," + winPoints[2]);
                    moveTile.HighlightWinner(winPoints, piece); //highlights winning piece
                    statsManager.SetScore(1, 0, 0, "YOU WIN"); // Updates tally
                    wld = WLD.win;
                    GameManager.GameState = false;
                    break;
                }
            }
            else if (piece == GameManager.Player2Piece)
            {
                if (board[winCombos[i, 0]] == piece && board[winCombos[i, 1]] == piece && board[winCombos[i, 2]] == piece)
                {
                    Debug.Log("<color=red> Player2 wins</color>");
                    winPoints[0] = winCombos[i, 0];
                    winPoints[1] = winCombos[i, 1];
                    winPoints[2] = winCombos[i, 2];
                    print("WIN POINTS: " + winPoints[0] + "," + winPoints[1] + "," + winPoints[2]);
                    moveTile.HighlightWinner(winPoints, piece);
                    statsManager.SetScore(0, 1, 0, "YOU LOSE");
                    wld = WLD.win;
                    GameManager.GameState = false;
                    break;
                }
            }

        }
        totalTurns++;
        if (totalTurns >= 9)
        {
            //Do something after all 9 turns have been taken
            CheckTie();
        }

    }

    /// <summary>
    /// Checks board for a tie if pieces exist in all points and no win or loss exists
    /// </summary>
    private void CheckTie()
    {
        if (wld != WLD.win && GameManager.GameState == true)
        {
            bool isEmpty = true;
            foreach (var point in board)
            {
                if (point == 'X' || point == 'O')
                {
                    isEmpty = !isEmpty;
                }
            }
            if (!isEmpty)
            {
                Debug.Log("<color=blue> Game ends in a Tie</color>");
                moveTile.HighlightWinner(winPoints, 'D');
                statsManager.SetScore(0, 0, 1, "DRAW");
                GameManager.GameState = false;
            }
        }


    }

    /// <summary>
    /// Umpire Ends game in win loss, tie or player reset
    /// </summary>
    public void GameOver()
    {
        //reset grid and board
        for (int i = 0; i < board.Length; i++)
        {
            board[i] = 'Z';

        }

        totalTurns = 0;
        wld = WLD.none;

        //Reset button

        GameManager.Player1Piece = 'X';
        gameEvents.InitialState("X"); //use current settings
        PointSelector.PointSelectorReset();
        moveTile.MoveTileReset();
        uIControl.UIControlReset();
        //Highlight Winning pieces

        //sound
        GameManager.GameState = true;
        //End gameplay states and reset logic
    }
}
