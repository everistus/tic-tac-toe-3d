﻿using System;
using System.Collections;
using UnityEngine;

//Simple tweening library

public static class TweenHelper
{
    static float t = 0;
    public static IEnumerator LinearLerp(GameObject obj, GameObject obj2, Action action = null)
    {

        while (true)
        {

            // obj.SetActive(true);
            // animate the position of the game object...
            obj.transform.position = new Vector2(obj.transform.position.x, Mathf.Lerp(obj.transform.position.y, obj2.transform.position.y, t));

            // .. and increate the t interpolater
            t += .5f * Time.deltaTime;

            if (t > .25f)
            {
                t = 0;
                action();
                break;
            }
            yield return null;

        }
    }
}




