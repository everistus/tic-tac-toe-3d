﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Board
{

    public char piece;
    public int point;
    public Board(char _piece, int _point)
    {
        piece = _piece;
        point = _point;
    }
}
