﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTile : MonoBehaviour
{

    public List<GameObject> currentPoints;
    [HideInInspector]
    public List<GameObject> emptyPoints;
    [HideInInspector]
    public List<GameObject> usedPoints;
    public GameObject[] player1Pieces;
    public GameObject[] player2Pieces;
    public GameObject[] spawnHolderPrefabs;
    public CPUHandMovement cpuHandMovement;

    public static MoveTile _instance = null;
    private MoveTile moveTile;
    private Umpire umpire;
    private GameEvents gameEvents;
    private AIBrain aiBrain;

    private int P1piecesUsed;
    private int P2piecesUsed;


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance = this)
        {
            Destroy(gameObject.GetComponent<MoveTile>());
        }

        DontDestroyOnLoad(gameObject);

    }


    // Use this for initialization
    void Start()
    {
        moveTile = MoveTile._instance;
        umpire = GetComponent<Umpire>();
        gameEvents = GetComponent<GameEvents>();
        aiBrain = GetComponent<AIBrain>();


    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Sets move properties during initialization or reset
    /// </summary>

    public void InitialMoveTileSettings()
    {
        emptyPoints = new List<GameObject>(currentPoints); // set to an instance of current points so it doesnt change the values of currentPoints

        // player1Pieces = new GameObject[5];
        //Player1 pieces intialize
        if (GameManager.Player1Piece == 'X')
        {
            player1Pieces = GameObject.FindGameObjectsWithTag("Xpiece");
            player2Pieces = GameObject.FindGameObjectsWithTag("Opiece");
        }
        else if (GameManager.Player1Piece == 'O')
        {
            player1Pieces = GameObject.FindGameObjectsWithTag("Opiece");
            player2Pieces = GameObject.FindGameObjectsWithTag("Xpiece");
        }

        P1piecesUsed = 0;
        P2piecesUsed = 0;
    }


    public void Player1MovetoPoint(string player, int point)
    {
        if (GameManager.GameState == true)
        {
            // Debug.Log("<color=green> point name:</color>" + point);
            int index = 0;
            if (P1piecesUsed < 5 && currentPoints.Count > 0)
            {
                for (int i = 0; i < currentPoints.Count; i++)
                {
                    if (currentPoints[i].name == point.ToString())
                    {
                        index = i;
                    }
                }
                //move piece to board
                player1Pieces[P1piecesUsed].transform.position = currentPoints[index].transform.position;
                player1Pieces[P1piecesUsed].transform.parent = currentPoints[index].transform;

                usedPoints.Add(currentPoints[index]);
                emptyPoints.Remove(currentPoints[index]);

                umpire.AddBoard(GameManager.Player1Piece, point); //Adds a piece on the board and checks for winner

                P1piecesUsed++;
                Player2MovetoPoint("CPU"); // use options
                GameManager.turn = GameManager.Turn.Player2Turn;
            }
        }


    }


    private int tempPoint;
    /// <summary>
    /// Sets the turn to a different player either Human or CPU
    /// </summary>
    /// <param name="player"></param>
    public void Player2MovetoPoint(string player)
    {
        if (GameManager.GameState == true)
        {
            if (player == "CPU")
            {

                if (P2piecesUsed < 5 && currentPoints.Count > 0)
                {
                    // Use Algorithm here
                    int val = 0;
                    val = aiBrain.Minimax().point;

                    int point = int.Parse(currentPoints[val].name);
                    tempPoint = point;
                    int index = 0;
                    for (int i = 0; i < currentPoints.Count; i++)
                    {
                        if (currentPoints[i].name == point.ToString())
                        {
                            index = i;
                            //Move Ai hand
                            StartCoroutine(cpuHandMovement.Lerp(null, currentPoints[index], moveTile.AIPlacePiece, index));

                            //  umpire.AddBoard(GameManager.Player2Piece, point); // Adds a piece on the board and check for winner
                            emptyPoints.Remove(currentPoints[index]);
                            usedPoints.Add(currentPoints[index]);

                            //turn off hover
                            currentPoints[index].GetComponent<PointSelector>().OnAIMouseDown();

                            // currentPoints.Remove(currentPoints[index]);
                            break;
                        }
                    }
                    P2piecesUsed++;

                }

            }
        }

    }

    /// <summary>
    /// Sets piece in slot after AI reaches its destination with couroutine
    /// </summary>
    /// <param name="index"></param>
    public void AIPlacePiece(int index)
    {
        //move piece to board
        //we get the point from the used list count-1. .. its latest addition
        player2Pieces[P2piecesUsed].transform.position = usedPoints[usedPoints.Count - 1].transform.position;
        player2Pieces[P2piecesUsed].transform.parent = usedPoints[usedPoints.Count - 1].transform;
        umpire.AddBoard(GameManager.Player2Piece, tempPoint); // Adds a piece on the board and check for winner
        SoundManager.playDrop();

    }

    /// <summary>
    /// Gets the winning points from the umpire and Changes its material properties
    /// </summary>
    /// <param name="_winPoints"></param>
    /// <param name="piece"></param>
    public void HighlightWinner(int[] _winPoints, char piece)
    {
        if (piece == GameManager.Player1Piece)
        {
            for (int i = 0; i < usedPoints.Count; i++)
            {
                if (usedPoints[i].name == _winPoints[0].ToString() || usedPoints[i].name == _winPoints[1].ToString() || usedPoints[i].name == _winPoints[2].ToString())
                {
                    usedPoints[i].transform.GetChild(0).GetComponent<MeshRenderer>().material = umpire.highLightMaterialP1; //highlights materials of winning pieces
                    usedPoints[i].GetComponent<PointSelector>().OnMouseExit(); // stops hover highlight after win
                }
            }
        }
        else if (piece == GameManager.Player2Piece)
        {
            for (int i = 0; i < usedPoints.Count; i++)
            {
                if (usedPoints[i].name == _winPoints[0].ToString() || usedPoints[i].name == _winPoints[1].ToString() || usedPoints[i].name == _winPoints[2].ToString())
                {
                    usedPoints[i].transform.GetChild(0).GetComponent<MeshRenderer>().material = umpire.highLightMaterialP2;
                    usedPoints[i].GetComponent<PointSelector>().OnMouseExit();
                }
            }
        }
        else if (piece == 'D')
        {
            for (int i = 0; i < usedPoints.Count; i++)
            {
                usedPoints[i].transform.GetChild(0).GetComponent<MeshRenderer>().material = umpire.highLightMaterialP3;
                usedPoints[i].GetComponent<PointSelector>().OnMouseExit();

            }
        }
    }

    /// <summary>
    /// Resets properties for move tile
    /// </summary>
    public void MoveTileReset()
    {
        P1piecesUsed = 0;
        P2piecesUsed = 0;
        //Remove and destroy all objects in used List
        do
        {
            for (int i = 0; i < usedPoints.Count; i++)
            {
                Destroy(usedPoints[i].transform.GetChild(0).gameObject);
                usedPoints.RemoveAt(i);
            }
        } while (usedPoints.Count > 0);

        //destroy all pieces and instantiate them again
        Destroy(gameEvents.Player1pieceSpawn.GetChild(0).gameObject);
        Destroy(gameEvents.Player2pieceSpawn.GetChild(0).gameObject);
        Instantiate(spawnHolderPrefabs[0], gameEvents.Player1pieceSpawn);
        Instantiate(spawnHolderPrefabs[1], gameEvents.Player2pieceSpawn);

    }
}
