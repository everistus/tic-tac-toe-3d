﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Prevent the hand from moving over UI elements and makes cursor visible
/// </summary>
public class UIHover : MonoBehaviour
{

    public HandMovement handMovement;


    public void MouseHover()
    {
        handMovement.isEnabled = false;
    }

    public void MouseOut()
    {
        handMovement.isEnabled = true;
    }
}
